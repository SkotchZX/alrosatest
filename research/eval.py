from utils.args_config_parse import *
from utils.determenistic import *
from agents import *
from efficientnet_pytorch import EfficientNet


def main():
    config = Dict(Fire(fit))
    set_determenistic()
    print(config)

    agent_class = globals()[config.agent]
    config.neptune = "turn_off"
    agent = agent_class(config)
    agent_model = agent_class.load_checkpoint()
    agent.model = agent_model.model.to(agent.device)
    print(agent.validate_one_epoch(0, True))


if __name__ == '__main__':
    main()
