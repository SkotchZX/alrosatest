import os

import xmltodict
import pandas as pd
from os.path import join
from os import listdir


def scraped_xml_dataset_preprocess(path_to_xml, path_to_files):
    with open(path_to_xml, 'r') as file:
        xml_dict = xmltodict.parse(file.read())

        num2class = {idx: data["name"] for idx, data in
                     enumerate(xml_dict["annotations"]["meta"]["task"]["labels"]["label"])}
        class2num = {v: k for k, v in num2class.items()}

        temp = [data["box"] if isinstance(data["box"], list) else [data["box"]] for data in
                xml_dict["annotations"]["image"]]
        labels = list(map(lambda x: list(map(lambda y: class2num[y["@label"]], x)), temp))

        temp = [data for data in xml_dict["annotations"]["image"]]
        filenames = list(map(lambda x: x["@name"].replace('rastenia_10032021/', ""), temp))

        data = list(zip(filenames, labels))
        data = list(filter(lambda x: len(x[1]) == 1 and x[1][0] not in [8, 9], data))
        data = list(map(lambda x: (join(path_to_files, x[0]), x[1][0]), data))
        return num2class, class2num, data


def zenudo_dataset_preprocess(path_to_csv, path_to_files):
    df = pd.read_csv(path_to_csv)

    num2class = {0: "healthy", 1: "ill"}
    class2num = {v: k for k, v in num2class.items()}

    data = [tuple(x) for x in df.values]
    data = list(map(lambda x: (join(path_to_files, x[0]), x[1] - 1), data))

    return num2class, class2num, data


def al_dataset_preprocess(path_to_folders):
    dirs = listdir(path_to_folders)
    num2class = {}
    class2num = {}
    data = []
    for idx, name in enumerate(dirs):
        num2class[idx] = name
        class2num[name] = idx
        long_path = join(path_to_folders, name)
        files = listdir(long_path)
        data += list(map(lambda x: (join(long_path, x), idx), files))
    return num2class, class2num, data
