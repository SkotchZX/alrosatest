import torch
import torch.utils.data as data
from PIL import Image
from os.path import join,normpath
from os import listdir, getcwd
from skimage import io
import numpy as np

import cv2
import xmltodict
from collections import Counter
from itertools import chain




def pad_to_square(bgr_image):
    desired_size = max(bgr_image.shape[0], bgr_image.shape[1])
    delta_w = desired_size - bgr_image.shape[1]
    delta_h = desired_size - bgr_image.shape[0]
    top, bottom = delta_h // 2, delta_h - (delta_h // 2)
    left, right = delta_w // 2, delta_w - (delta_w // 2)

    color = [0, 0, 0]
    bgr_image = cv2.copyMakeBorder(bgr_image, top, bottom, left, right, cv2.BORDER_REFLECT_101,
                                   value=color)
    return bgr_image


class ClassicDataset(data.Dataset):
    def __init__(self, pairs, transforms=None):
        super(ClassicDataset, self).__init__()
        self.transforms = transforms
        self.data = pairs

    def __getitem__(self, index):
        path = self.data[index][0]
        sample = np.array(Image.open(path))

        sample_target = self.data[index][1]
        if self.transforms:
            sample = self.transforms(image=sample)
        return sample["image"], sample_target, path

    def __len__(self):
        return len(self.data)
