import torch.backends, torch.cuda
import random
import numpy as np


def set_determenistic(seed=42, precision=10):
    np.random.seed(seed)
    random.seed(seed)
    # torch.backends.cudnn.bechmark = False       # Possible source of numerical instability
    # torch.backends.cuddn.deterministics = True  # Possible source of numerical instability
    torch.cuda.manual_seed_all(seed)
    torch.manual_seed(seed)
    torch.set_printoptions(precision=precision)
