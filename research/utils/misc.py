import time
import research_logging
from os.path import basename, join, exists
from os import mkdir
from shutil import copyfile, rmtree
from sklearn.metrics import confusion_matrix
from utils.confution_pretty_print.pretty_print import pretty_plot_confusion_matrix
import numpy as np
import pandas as pd


def clear_folder(path):
    if exists(path):
        rmtree(path)
    mkdir(path)


def save_missmatch_artifact(targets, outputs, file_paths, artifacts_path, num_classes):
    clear_folder(artifacts_path)

    matr = confusion_matrix(targets, outputs)
    df_cm = pd.DataFrame(matr, index=range(num_classes), columns=range(num_classes))
    pretty_plot_confusion_matrix(df_cm, file_to_save=join(artifacts_path, "matr.jpg"))

    targets = np.array(targets)
    outputs = np.array(outputs)
    file_paths = np.array(file_paths)

    idx = targets != outputs
    targets = targets[idx]
    outputs = outputs[idx]
    file_paths = file_paths[idx]

    for target, output, src_path in zip(targets, outputs, file_paths):
        base = basename(src_path)
        dst_path = join(artifacts_path, f"t{target}o{output}" + base)
        copyfile(src_path, dst_path)



def timeit(f):
    """ Decorator to time Any Function """

    def timed(*args, **kwargs):
        start_time = time.time()
        result = f(*args, **kwargs)
        end_time = time.time()
        seconds = end_time - start_time
        research_logging.getLogger("Timer").info("   [-] %s : %2.5f sec, which is %2.5f min, which is %2.5f hour" %
                                                 (f.__name__, seconds, seconds / 60, seconds / 3600))
        return result

    return timed


def print_cuda_statistics():
    logger = research_logging.getLogger("Cuda Statistics")
    import sys
    from subprocess import call
    import torch
    logger.info('__Python VERSION:  {}'.format(sys.version))
    logger.info('__pyTorch VERSION:  {}'.format(torch.__version__))
    logger.info('__CUDA VERSION')
    call(["nvcc", "--version"])
    logger.info('__CUDNN VERSION:  {}'.format(torch.backends.cudnn.version()))
    logger.info('__Number CUDA Devices:  {}'.format(torch.cuda.device_count()))
    logger.info('__Devices')
    call(["nvidia-smi", "--format=csv",
          "--query-gpu=index,name,driver_version,memory.total,memory.used,memory.free"])
    logger.info('Active CUDA Device: GPU {}'.format(torch.cuda.current_device()))
    logger.info('Available devices  {}'.format(torch.cuda.device_count()))
    logger.info('Current cuda device  {}'.format(torch.cuda.current_device()))
