from utils.args_config_parse import *
from utils.determenistic import *
from agents import *
from efficientnet_pytorch import EfficientNet


def main():
    config = Dict(Fire(fit))
    set_determenistic()
    print(config)

    agent_class = globals()[config.agent]
    agent = agent_class(config)
    agent.run_train()
    agent.finalize()



if __name__ == '__main__':
    main()
