from __future__ import print_function

import ast
from web3 import Web3
import requests


def main():
    test_url = "http://192.168.0.11:5000/api/test"

    img = open("imgs_for_request/Tomato_healthy.JPG", 'rb').read()
    request_json = {"img": Web3.toHex(img)}
    num2classes = {0: 'Tomato_Bacterial_spot',
                   1: 'Tomato__Tomato_YellowLeaf__Curl_Virus',
                   2: 'Tomato_Septoria_leaf_spot',
                   3: 'Tomato_Early_blight',
                   4: 'Tomato_Late_blight',
                   5: 'Tomato_healthy'}

    res = requests.get(test_url, json=request_json)
    res = ast.literal_eval(res.content.decode("utf-8"))["response"]
    print("Класс данного изображения: ", num2classes[res])


if __name__ == '__main__':
    main()
