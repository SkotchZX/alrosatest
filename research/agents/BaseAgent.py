import numpy as np
import torch
import torch.nn
from torch.utils.data import DataLoader
from datasets.classic_dataset import ClassicDataset
from datasets.preprocess import scraped_xml_dataset_preprocess, zenudo_dataset_preprocess, al_dataset_preprocess
from research_logging.combo_logging import ComboLogger
from sklearn.model_selection import train_test_split
from collections import Counter
from sklearn.preprocessing import normalize
import albumentations as A
import albumentations.pytorch as Ap

# тренировочные аугментации
transforms_train = A.Compose([
    A.RGBShift(r_shift_limit=15, g_shift_limit=15,  # сдвиг в цветовом пространстве
               b_shift_limit=15, p=0.5),
    A.VerticalFlip(p=0.5),  # поворот на случайный угол и вертикальное отражение
    A.HorizontalFlip(p=0.5),  # поворот на случайный угол и горизонтальное отражение
    A.Rotate(limit=90, p=1),  # поворот на случайный угол
    A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], max_pixel_value=255),
    A.Resize(width=240, height=240),  # изменение размера в 240x240 пикселей
    Ap.ToTensorV2()
])

transforms_val = A.Compose([
    A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225], max_pixel_value=255),
    A.Resize(width=240, height=240),
    Ap.ToTensorV2()
])


class BaseAgent:
    """
    This base class will contain the common features for Agents you will implement.
    """

    def __init__(self, cfg):
        self.config = cfg
        self.logger = ComboLogger(cfg)

        self.current_epoch = 0
        self.current_iteration = 0
        self.best_metric = 0

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.summary_writer = None
        self.best_valid_loss = None

        if not cfg.is_application_mode:
            self.num2class, self.class2num, data = al_dataset_preprocess("../Tomato")

            classes = list(map(lambda x: x[1], data))
            classes_count = np.array([list(map(lambda x: x[1], sorted(Counter(classes).items())))])

            # весовая корректировка для баланса классов
            self.classes_weights = torch.tensor(normalize(1 / classes_count)[0, :]).to(self.device).float()
            self.num_classes = self.classes_weights.shape[0]

            # стратифицированное разбиение выборки
            x_train, x_val, y_train, y_val = train_test_split(data, classes, test_size=0.3, random_state=42,
                                                              stratify=classes)
            classes = list(map(lambda x: x[1], x_val))
            x_val, x_test, y_val, y_test = train_test_split(x_val, classes, test_size=0.5, random_state=42,
                                                            stratify=classes)

            self.train_data_loader = ClassicDataset(pairs=x_train, transforms=transforms_train)
            self.val_data_loader = ClassicDataset(pairs=x_val, transforms=transforms_val)
            self.test_data_loader = ClassicDataset(pairs=x_val, transforms=transforms_val)
            self.train_data_loader = DataLoader(self.train_data_loader, shuffle=True, batch_size=cfg.batch_size,
                                                num_workers=12, drop_last=True)
            self.val_data_loader = DataLoader(self.val_data_loader, shuffle=True, batch_size=cfg.batch_size,
                                              num_workers=12, drop_last=True)  # drop_last=True fixing cuda out of memory
            self.test_data_loader = DataLoader(self.test_data_loader, shuffle=True, batch_size=cfg.batch_size,
                                               num_workers=12, drop_last=True)  # drop_last=True fixing cuda out of memory
        else:
            self.num_classes = 6

    def load_checkpoint(self, file_name):
        """
        Latest checkpoint loader
        :param file_name: name of the checkpoint file
        :return:
        """
        raise NotImplementedError

    def save_checkpoint(self, file_name="checkpoint.pth.tar", is_best=0):
        """
        Checkpoint saver
        :param file_name: name of the checkpoint file
        :param is_best: boolean flag to indicate whether current checkpoint's metric is the best so far
        :return:
        """
        raise NotImplementedError

    def run_train(self):
        """
        The main operator
        :return:
        """
        raise NotImplementedError

    def train(self):
        """
        Main training loop
        :return:
        """
        raise NotImplementedError

    def train_one_epoch(self, cur_epoch):
        """
        One epoch of training
        :return:
        """
        raise NotImplementedError

    def validate_one_epoch(self, cur_epoch, is_on_test=False):
        """
        One cycle of model validation
        :return:
        """
        raise NotImplementedError

    def finalize(self):
        """
        Finalizes all the operations of the 2 Main classes of the process, the operator and the data loader
        :return:
        """
        raise NotImplementedError
