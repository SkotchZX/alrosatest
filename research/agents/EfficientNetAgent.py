import numpy as np
import pandas as pd
from torchvision.transforms import transforms
from tqdm import tqdm

import torch
from torch import nn
from torch.backends import cudnn
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
import torch.nn
from agents.BaseAgent import BaseAgent, transforms_val
from efficientnet_pytorch import EfficientNet
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix

from utils.misc import save_missmatch_artifact
from agents import *
from efficientnet_pytorch import EfficientNet
from torch import load, save

cudnn.benchmark = True


class EfficientNetAgent(BaseAgent):
    def __init__(self, cfg):
        BaseAgent.__init__(self, cfg)

        self.model_reconfigure(num_classes=self.num_classes) # Конфигурация модели
        if hasattr(self, "classes_weights"):
            self.loss = nn.CrossEntropyLoss(weight=self.classes_weights)  # CrossEntropyLoss - целевая функция для обучения
        else:
            self.loss = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(self.model.parameters(), lr=self.config.learning_rate,
                                   momentum=self.config.momentum,
                                   weight_decay=self.config.weight_decay) # стохастический градиентный спуск
        self.scheduler = lr_scheduler.StepLR(self.optimizer, step_size=cfg.step_size, gamma=cfg.gamma)

    def model_reconfigure(self, num_classes):
        self.model = EfficientNet.from_pretrained('efficientnet-b0')  # Претренированная на imagenet-е efficientnet
        self.model._fc = nn.Linear(self.model._fc.in_features, num_classes)
        for name, child in self.model.named_children():
            if name in ['_conv_stem', '_bn0']:
                for param in child.parameters():
                    param.requires_grad = False
            elif name == "_blocks":
                for name2, child2 in child.named_children():
                    if int(name2) < 5:
                        for param in child2.parameters():
                            param.requires_grad = False
                    else:
                        for param in child2.parameters():
                            param.requires_grad = True
            else:
                for param in child.parameters():
                    param.requires_grad = True
        self.model = self.model.to(self.device)

    @staticmethod
    def load_checkpoint(file_name="models_weights/best_checkpoint.pth.tar", device="cpu"):
        return torch.load(file_name, map_location=torch.device(device))

    def save_checkpoint(self, is_best, file_name="checkpoint.pth.tar"):
        torch.save(self, "models_weights/last_" + file_name)
        if is_best:
            torch.save(self, "models_weights/best_" + file_name)

    def save_model(self, is_best, file_name="model.pth.tar"):
        prefix = "best" if is_best else "last"
        torch.save(self.model, f"models_weights/{prefix}_{file_name}")

    def run_train(self):
        """
                The main operator
                :return:
                """
        try:
            self.train()

        except KeyboardInterrupt:
            print("You have entered CTRL+C.. Wait to finalize")

    def train(self):
        for epoch in range(self.current_epoch, self.config.max_epoch):
            print(f"Epoch {epoch} is started")
            if self.best_valid_loss is None:
                self.best_valid_loss = 1000000
            self.current_epoch = epoch

            training_loss, training_metrics = self.train_one_epoch(epoch)
            self.logger.info("train loss", epoch, training_loss)
            for key, val in training_metrics.items():
                self.logger.info(key, epoch, val)

            valid_loss, valid_metrics = self.validate_one_epoch(epoch)
            self.logger.info("valid loss", epoch, valid_loss)
            for key, val in valid_metrics.items():
                self.logger.info(key, epoch, val)

            self.scheduler.step()
            is_best = valid_loss < self.best_valid_loss
            if is_best:
                self.best_valid_loss = valid_loss

            self.save_checkpoint(is_best=is_best)

    def train_one_epoch(self, epoch):
        """
        One epoch of training
        :return:
        """

        self.model.train()
        running_loss = 0
        running_batch_size = 0
        accum_outputs = []
        accum_targets = []
        for idx, (data, target, file_path) in tqdm(enumerate(self.train_data_loader),
                                                   total=len(self.train_data_loader)):
            data, target = data.to(self.device), target.to(self.device)
            self.optimizer.zero_grad()
            with torch.set_grad_enabled(True):
                output = self.model(data)
                loss = self.loss(output, target)
                loss.backward()
                self.optimizer.step()
                running_loss += loss.item() * data.size(0)
                running_batch_size += data.size(0)
            accum_targets += list(target.data.cpu().numpy())
            accum_outputs += list(torch.argmax(output, dim=1).data.cpu().numpy())

        final_loss = running_loss / running_batch_size
        training_metrics = {"train_accuracy": accuracy_score(accum_targets, accum_outputs),
                            "train_macro_f1": f1_score(accum_targets, accum_outputs, average="macro")}
        return final_loss, training_metrics

    def validate_one_epoch(self, cur_epoch, is_on_test=False):
        self.model.eval()
        running_loss = 0
        running_batch_size = 0
        accum_outputs = []
        accum_targets = []
        accum_paths = []
        current_loader = self.test_data_loader if is_on_test else self.val_data_loader
        for idx, (data, target, file_path) in tqdm(enumerate(current_loader), total=len(current_loader)):
            data, target = data.to(self.device), target.to(self.device)
            with torch.no_grad():
                output = self.model(data)
                loss = self.loss(output, target)
                running_loss += loss.item() * data.size(0)
                running_batch_size += data.size(0)
            accum_targets += list(target.data.cpu().numpy())
            accum_outputs += list(torch.argmax(output, dim=1).data.cpu().numpy())
            accum_paths += file_path

        final_loss = running_loss / running_batch_size
        accuracy_metrics = {"val_accuracy": accuracy_score(accum_targets, accum_outputs),
                            "val_macro_f1": f1_score(accum_targets, accum_outputs, average="macro")}
        if is_on_test:
            conf_matr = confusion_matrix(y_true=accum_targets, y_pred=accum_outputs)
            print(conf_matr)
        return final_loss, accuracy_metrics

    def infer_one_image(self, image):
        self.model.eval()
        aug_pipe = transforms_val
        image = torch.unsqueeze(aug_pipe(image=np.array(image))["image"], 0).to(self.device)
        with torch.no_grad():
            output = self.model(image)
            predict = torch.argmax(output, dim=1).data.cpu().numpy()
            return predict
