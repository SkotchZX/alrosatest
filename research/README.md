# Тестовое задание 

Ссылка на телеграмм бота: https://t.me/testalrosabot <br>
Ссылка на датасет: https://www.kaggle.com/sohaibalam67/tomato-disease <br>

Ссылка на графики обучения модели на neptune.ai: <br>
https://app.neptune.ai/maruo95/testalrosa/e/TES-10/dashboard/my-dash-3b416913-5145-4ae9-8af0-bd0ce5d127f6<br>

скрипт для обучения модели - train.py <br>
скрипт для оценки модели - eval.py <br>

rest-api сервис - app.py <br>
скрипт для обращения к сервису - request.py <br>

Метрики на тестовой подвыборке: <br>
accuracy: 0.985 <br>
macro_f1: 0.983 <br>
