import io
from os.path import join

import PIL
from PIL import Image
import torch

from flask import Flask, request, Response, jsonify
import numpy as np
import json
from web3 import Web3

from utils.args_config_parse import *
from utils.determenistic import *
from agents import *
from efficientnet_pytorch import EfficientNet


app = Flask(__name__)
device = "cuda" if torch.cuda.is_available() else "cpu"

config = Dict(Fire(fit))
set_determenistic()
print(config)

agent_class = globals()[config.agent]
config.neptune = "turn_off"
config.is_application_mode = True
agent = agent_class(config)
agent_model = agent_class.load_checkpoint()
agent.model = agent_model.model


# route http posts to this method
@app.route('/api/test', methods=['GET'])
def test():
    r = request
    data = json.loads(r.data.decode("utf-8"))
    data = Web3.toBytes(hexstr=data["img"])
    img = PIL.Image.open(io.BytesIO(data))
    result = int(agent_model.infer_one_image(img)[0])
    return {"response": result}


# start flask app
app.run(host="0.0.0.0", port=5000)
