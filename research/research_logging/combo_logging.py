from research_logging.console_logging import *
from research_logging.neptune_logging import *


class ComboLogger:
    def __init__(self, config):
        self.config = config
        if self.config.neptune == "turn_on":
            self.neptune_logger = NeptuneLogger(config)
        self.console_logger = ConsoleLogger(config)

    def info(self, metric_name, epoch, metric_value):
        self.console_logger.info(metric_name, epoch, metric_value)
        if self.config.neptune == "turn_on":
            self.neptune_logger.info(metric_name, epoch, float(metric_value))
