class ConsoleLogger:
    def __init__(self, config):
        self.config = config

    def info(self, metric_name, epoch, metric_value):
        print(f"{metric_name}: {metric_value}")
