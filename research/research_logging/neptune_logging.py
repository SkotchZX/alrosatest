import neptune
from time import sleep


class NeptuneLogger:
    def __init__(self, config):
        neptune.init(project_qualified_name='maruo95/testalrosa',
             api_token='eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiZTYxMzNmZDMtMTA1Yi00Yjk5LTg4MzktNDkyN2M5YjJkZjE0In0=')
        neptune.create_experiment(name=config.experiment_name, params=config,
                                  upload_source_files=config.experiment_tags)
        neptune.append_tags('efficientNet', 'classic')

    def info(self, metric_name, epoch, metric_value):
        neptune.log_metric(metric_name, epoch, y=metric_value)

# neptune.init(project_qualified_name='maruo95/plantsDiseases',
#              api_token='eyJhcGlfYWRkcmVzcyI6Imh0dHBzOi8vdWkubmVwdHVuZS5haSIsImFwaV91cmwiOiJodHRwczovL3VpLm5lcHR1bmUuYWkiLCJhcGlfa2V5IjoiZTYxMzNmZDMtMTA1Yi00Yjk5LTg4MzktNDkyN2M5YjJkZjE0In0=')
# neptune.create_experiment(name="Plants Diseases")
# neptune.append_tags('efficientNet', 'classic')
#
# for i in range(100):
#     k = i * 2/3
#     neptune.log_metric("my_metric_loss", k + 1)
#     neptune.log_metric("my_metric_acc", k + 2)
#     neptune.log_metric("my_metric_f1", k + 3)
#     sleep(15)
#     neptune.log_metric("my_metric_val_loss", 11 + k)
#     neptune.log_metric("my_metric_val_acc", 13 + k)
#     neptune.log_metric("my_metric_val_f1", 14 + k)
#     sleep(35)