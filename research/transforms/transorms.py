import torchvision, torch
import random
import numpy as np

from skimage import transform as sk_transf
from skimage import util as sk_util
from skimage import data, io
from matplotlib import pyplot as plt


def compose_transforms(transforms_configs):
    """ Create list of transforms from their names"""
    list_of_transforms = []
    for transform in transforms_configs:
        transform_class = globals()[transform]
        transform_object = transform_class()  # Добавить возможность конфигурировать трансформ
        list_of_transforms.append(transform_object)

    return torchvision.transforms.Compose(list_of_transforms)


class RandHReflectTrans(object):
    """Apply horizontal flip to image with 50% chance"""
    def __call__(self, sample):
        return np.flip(sample, 1) if random.randint(0, 1) == 0 else sample


class Pad2SquareTrans(object):
    """Apply padding to get square image"""
    def __call__(self, sample):
        max_shape_size = np.max(sample.shape)
        new_h = abs(max_shape_size - sample.shape[0])
        new_w = abs(max_shape_size - sample.shape[1])
        return np.pad(sample, ((0, new_h), (0, new_w), (0, 0)), "constant")


class ReshapeTrans(object):
    """Reshape image to fit resnet18"""
    def __call__(self, sample):
        return sk_transf.resize(sample, (224, 224))


class ToTensorTrans(object):
    """Convert image to tensor"""
    def __call__(self, sample):
        # io.imshow(sample)
        # plt.show()
        temp = sample.transpose((2, 0, 1))
        return torch.from_numpy(temp)
