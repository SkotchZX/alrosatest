import os
from io import BytesIO
import agents
import telebot
import torch
from PIL import Image
from utils.args_config_parse import *
from utils.determenistic import *
from agents import *
from efficientnet_pytorch import EfficientNet

TOKEN = "1879277787:AAFu2ZjbTjTaaeNDONDRDuBj5YHE4i_kcbI"

bot = telebot.TeleBot(TOKEN)

device = "cuda" if torch.cuda.is_available() else "cpu"

config = Dict(Fire(fit))
set_determenistic()
print(config)

agent_class = globals()[config.agent]
config.neptune = "turn_off"
config.is_application_mode = True
agent = agent_class(config)
agent_model = agent_class.load_checkpoint(device=device)
agent.model = agent_model.model


@bot.message_handler(content_types=['text'])
def text_response(message):
    bot.send_message(message.chat.id, "Привет, всё, что я умею делать - это распозновать литья томатов на разные "
                                      "болезни. Пожалуйста, отправьте мне изображения из тестового набора!")


num2classes = {0: 'Tomato_Bacterial_spot',
               1: 'Tomato__Tomato_YellowLeaf__Curl_Virus',
               2: 'Tomato_Septoria_leaf_spot',
               3: 'Tomato_Early_blight',
               4: 'Tomato_Late_blight',
               5: 'Tomato_healthy'}


@bot.message_handler(content_types=['document', 'photo'])
def image_response(message):
    try:
        file_id_info = bot.get_file(message.photo[-1].file_id)
        downloaded_file = bot.download_file(file_id_info.file_path)
        stream = BytesIO(downloaded_file)
        img = Image.open(stream).convert("RGB")
        result = int(agent.infer_one_image(img)[0])
        result = num2classes[result]
        bot.send_message(message.chat.id, f"Я выяснил, что это изображение принадлежит к классу: {result}")
    except Exception as ex:
        bot.send_message(message.chat.id, "[!] error - {}".format(str(ex)))


bot.polling(none_stop=True)
